# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  # Do not automatically update boxes
  config.vm.box_check_update = false

  # Do not automatically update VirtualBox Guest Additions
  if Vagrant.has_plugin?("vagrant-vbguest") then
    config.vbguest.auto_update = false
  end

  # For all VirtualBox machines
  config.vm.provider "virtualbox" do |vb|
    # Disable GUI
    vb.gui = false
    # Disable audio controller
    vb.customize ["modifyvm", :id, '--audiocontroller', 'AC97', '--audio', 'Null']
    # Use linked clones
    vb.linked_clone = true
  end

  # ISP router (Cumulus VX)
  config.vm.define "isp" do |node|
    node.vm.hostname = "cumulus-isp"
    node.vm.box = "CumulusCommunity/cumulus-vx"
    node.vm.box_version = "3.7.11"
    node.vm.provider "virtualbox" do |vb|
      vb.name = "cumulus-isp"
      # Recommended RAM
      vb.memory = "768"
      # Fix NAT (https://github.com/hashicorp/vagrant/issues/2779)
      vb.customize ["modifyvm", :id, "--nic2", "nat"]
    end
    node.vm.network "private_network", type: "dhcp"
    node.vm.network "private_network", ip: "192.168.191.10", virtualbox__intnet: "internet"
    node.vm.network "private_network", ip: "192.168.192.10", virtualbox__intnet: "provider"
    # Provision via shell script
    node.vm.provision "shell", path: "isp/provision.sh"
  end

  # WAN emulator (VyOS)
  config.vm.define "wan" do |node|
    node.vm.hostname = "vyos-wan"
    node.vm.box = "samdoran/vyos"
    node.vm.box_version = "1.1.1"
    node.vm.provider "virtualbox" do |vb|
      vb.name = "vyos-wan"
      vb.memory = "256"
      # Allow promiscious node to be able to bridge the interfaces
      vb.customize ['modifyvm', :id, '--nicpromisc2', 'allow-all']
      vb.customize ['modifyvm', :id, '--nicpromisc3', 'allow-all']
    end
    # IP addresses not configured or used
    node.vm.network "private_network", ip: "192.168.192.11", auto_config: false, virtualbox__intnet: "provider"
    node.vm.network "private_network", ip: "192.168.192.12", auto_config: false, virtualbox__intnet: "customer"
    # Upload WAN script
    node.vm.provision "file", source: "wan/WAN.sh", destination: "/home/vagrant/WAN.sh"
    node.vm.provision "shell", inline: "chmod +x /home/vagrant/WAN.sh"
    # Provision via shell script
    node.vm.provision "shell", path: "wan/provision.sh"
  end

  # CE router (Cumulus VX)
  config.vm.define "ce" do |node|
    node.vm.hostname = "cumulus-ce"
    node.vm.box = "CumulusCommunity/cumulus-vx"
    node.vm.box_version = "3.7.11"
    node.vm.provider "virtualbox" do |vb|
      vb.name = "cumulus-ce"
      # Recommended RAM
      vb.memory = "768"
    end
    node.vm.network "private_network", ip: "192.168.192.20", virtualbox__intnet: "customer"
    node.vm.network "private_network", ip: "192.168.193.10", virtualbox__intnet: "web"
    node.vm.network "private_network", ip: "192.168.194.10", virtualbox__intnet: "sflow"
    # Provision via shell script
    node.vm.provision "shell", path: "ce/provision.sh"
  end

  # Web server 1 (Ubuntu)
  config.vm.define "web1" do |node|
    node.vm.hostname = "web1"
    node.vm.box = "ubuntu/bionic64"
    node.vm.box_version = "20200131.0.0"
    node.vm.provider "virtualbox" do |vb|
      vb.name = "web1"
      # Minimum RAM
      vb.memory = "256"
    end
    node.vm.network "private_network", ip: "192.168.193.20", virtualbox__intnet: "web"
    # Provision via shell script
    node.vm.provision "shell", path: "web/provision.sh"
    # Forward port 80:8001
    node.vm.network "forwarded_port", guest: 80, host: 8001, host_ip: "127.0.0.1"
  end

  # Web server 2 (Ubuntu)
  config.vm.define "web2" do |node|
    node.vm.hostname = "web2"
    node.vm.box = "ubuntu/bionic64"
    node.vm.box_version = "20200131.0.0"
    node.vm.provider "virtualbox" do |vb|
      vb.name = "web2"
      # Minimum RAM
      vb.memory = "256"
    end
    node.vm.network "private_network", ip: "192.168.193.30", virtualbox__intnet: "web"
    # Provision via shell script
    node.vm.provision "shell", path: "web/provision.sh"
    # Forward port 80:8002
    node.vm.network "forwarded_port", guest: 80, host: 8002, host_ip: "127.0.0.1"
  end

  # sFlow-RT server (Ubuntu)
  config.vm.define "sflow" do |node|
    node.vm.hostname = "sflow"
    node.vm.box = "ubuntu/bionic64"
    node.vm.box_version = "20200131.0.0"
    node.vm.provider "virtualbox" do |vb|
      vb.name = "sflow"
      # Additional RAM for sFlow-RT (1024 for Java)
      vb.memory = "1536"
    end
    node.vm.network "private_network", ip: "192.168.194.20", virtualbox__intnet: "sflow"
    # Provision via shell script
    node.vm.provision "shell", path: "sflow/provision.sh"
    # Forward port 8008:8008
    node.vm.network "forwarded_port", guest: 8008, host: 8008, host_ip: "127.0.0.1"
  end

  # Test user machine (Ubuntu)
  config.vm.define "user" do |node|
    node.vm.hostname = "user"
    node.vm.box = "ubuntu/bionic64"
    node.vm.box_version = "20200131.0.0"
    node.vm.provider "virtualbox" do |vb|
      vb.name = "user"
      # Minimum RAM
      vb.memory = "256"
    end
    node.vm.network "private_network", ip: "192.168.191.20", virtualbox__intnet: "internet"
    # Upload check script
    node.vm.provision "file", source: "web/check.sh", destination: "/home/vagrant/check.sh"
    node.vm.provision "shell", inline: "chmod +x /home/vagrant/check.sh"
    # Provision via shell script
    node.vm.provision "shell", path: "user/provision.sh"
  end

  # Attack machine (Ubuntu)
  config.vm.define "attack" do |node|
    node.vm.hostname = "attack"
    node.vm.box = "ubuntu/bionic64"
    node.vm.box_version = "20200131.0.0"
    node.vm.provider "virtualbox" do |vb|
      vb.name = "attack"
      # Minimum RAM
      vb.memory = "256"
    end
    node.vm.network "private_network", ip: "192.168.191.30", virtualbox__intnet: "internet"
    # Upload attack scripts
    node.vm.provision "file", source: "attack/attack1.sh", destination: "/home/vagrant/attack1.sh"
    node.vm.provision "shell", inline: "chmod +x /home/vagrant/attack1.sh"
    node.vm.provision "file", source: "attack/attack2.sh", destination: "/home/vagrant/attack2.sh"
    node.vm.provision "shell", inline: "chmod +x /home/vagrant/attack2.sh"
    node.vm.provision "file", source: "attack/attack3.sh", destination: "/home/vagrant/attack3.sh"
    node.vm.provision "shell", inline: "chmod +x /home/vagrant/attack3.sh"
    # Provision via shell script
    node.vm.provision "shell", path: "attack/provision.sh"
  end

end
