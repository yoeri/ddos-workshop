#!/bin/bash

# Install packages
apt-get update
apt-get install -y docker.io openjdk-11-jre

# Create aliases
echo "### Added by Vagrant provisioning" >> /root/.profile
echo "alias sflowtool='docker run -p 6343:6343/udp sflow/sflowtool'" >> /root/.profile
echo "alias start-sflow='screen -S sFlow-RT -t sFlow-RT -d -m /root/sflow-rt/start.sh'" >> /root/.profile
echo "alias stop-sflow='ps aux | grep -i screen.*sFlow-RT | grep -v grep | awk \"{print $2}\" | xargs kill'"  >> /root/.profile 
echo "### ---" >> /root/.profile

# Prepare sFlow tool (for troubleshooting)
docker run -p 6343:6343/udp sflow/sflowtool -h

# Install sFlow-RT
#wget https://inmon.com/products/sFlow-RT/sflow-rt_2.3-1377.deb
#dpkg -i sflow-rt_2.3-1377.deb
wget https://inmon.com/products/sFlow-RT/sflow-rt_3.0-1462.deb
dpkg -i sflow-rt_3.0-1462.deb

# Get sFlow-RT apps
/usr/local/sflow-rt/get-app.sh sflow-rt active-routes
/usr/local/sflow-rt/get-app.sh sflow-rt ddos-blackhole
/usr/local/sflow-rt/get-app.sh sflow-rt flow-trend
/usr/local/sflow-rt/get-app.sh sflow-rt sflow-test
/usr/local/sflow-rt/get-app.sh sflow-rt top-flows
/usr/local/sflow-rt/get-app.sh sflow-rt dashboard-example	
/usr/local/sflow-rt/get-app.sh sflow-rt flow-graph
/usr/local/sflow-rt/get-app.sh sflow-rt trace-flow

# Configure sFlow-RT
cat >> /usr/local/sflow-rt/conf.d/sflow-rt.conf <<EOF
bgp.start=yes
ddos_blackhole.router=192.168.194.10
ddos_blackhole.as=65001
EOF

# Configure DDoS app
cp /usr/local/sflow-rt/app/ddos-blackhole/scripts/ddos.js /usr/local/sflow-rt/app/ddos-blackhole/scripts/ddos.js.bekup
sed -i -e "s#private:\['10.0.0.0/8','172.16.0.0/12','192.168.0.0/16','169.254.0.0/16'\],#private:['192.168.192.0/24','192.168.193.0/24','192.168.194.0/24'],#" /usr/local/sflow-rt/app/ddos-blackhole/scripts/ddos.js
sed -i -e "s#multicast:\['224.0.0.0/4'\]#multicast:['224.0.0.0/4'],\n  web:['192.168.193.0/24']#" /usr/local/sflow-rt/app/ddos-blackhole/scripts/ddos.js
sed -i -e "s#var threshold = .* 1000000;#var threshold = storeGet('threshold')         || 5000;#" /usr/local/sflow-rt/app/ddos-blackhole/scripts/ddos.js
sed -i -e "s#var block_minutes = .* 60;#var block_minutes = storeGet('block_minutes') || 5;#" /usr/local/sflow-rt/app/ddos-blackhole/scripts/ddos.js

# (Re)start sFlow-RT
service sflow-rt restart

## Set default route via CE router
#sudo route delete -net 0.0.0.0/0
#sudo route add -net 0.0.0.0/0 gw 192.168.194.10

