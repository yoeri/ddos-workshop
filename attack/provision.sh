#!/bin/bash

# Ensure non-interactive apt-get
export DEBIAN_FRONTEND=noninteractive

# Install packages
apt-get update
apt-get install -y hping3 iperf iperf3 python3-pip dos2unix

# Fix new lines on Windows hosts
dos2unix /home/vagrant/*.sh

# Install attack
pip3 install slowloris

## Set default route via ISP router
#sudo route delete -net 0.0.0.0/0
#sudo route add -net 0.0.0.0/0 gw 192.168.191.10

# Set route to web server via ISP router
sudo route add -net 192.168.193.0/24 gw 192.168.191.10

# Make route to web server via ISP router permanent
sudo touch /etc/rc.local
sudo sed -i -e 's/exit 0/#exit 0/' /etc/rc.local
echo "#!/bin/sh -e" | sudo tee -a /etc/rc.local
echo "sudo route add -net 192.168.193.0/24 gw 192.168.191.10" | sudo tee -a /etc/rc.local
echo "exit 0" | sudo tee -a /etc/rc.local
sudo chmod +x /etc/rc.local

