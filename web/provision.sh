#!/bin/bash

# Ensure non-interactive apt-get (for tshark)
export DEBIAN_FRONTEND=noninteractive

# Install packages
apt-get update
apt-get install -y iperf iperf3 tshark apache2

# Enable /server-status from host
sed -i -e 's/Require local/#Require local/' /etc/apache2/mods-enabled/status.conf

# Disable Slowloris protection
sed -i -e 's/^/#/' /etc/apache2/mods-enabled/reqtimeout.load

# Restart Apache
systemctl restart apache2

## Set default route via CE router
#sudo route delete -net 0.0.0.0/0
#sudo route add -net 0.0.0.0/0 gw 192.168.193.10

# Set route to user/attacker via CE router
sudo route add -net 192.168.191.0/24 gw 192.168.193.10

# Set route to isp/ce link via CE router
sudo route add -net 192.168.192.0/24 gw 192.168.193.10

# Make routes permanent
sudo touch /etc/rc.local
sudo sed -i -e 's/exit 0/#exit 0/' /etc/rc.local
echo "#!/bin/sh -e" | sudo tee -a /etc/rc.local
echo "sudo route add -net 192.168.191.0/24 gw 192.168.193.10" | sudo tee -a /etc/rc.local
echo "sudo route add -net 192.168.192.0/24 gw 192.168.193.10" | sudo tee -a /etc/rc.local
echo "exit 0" | sudo tee -a /etc/rc.local
sudo chmod +x /etc/rc.local

