#!/bin/bash

# Continuously check Web 1 and Web 2 (with 5 second timeout)

while true; do
  WEB1=DOWN
  if curl -s -m 5 http://192.168.193.20 >/dev/null; then
    WEB1=UP
  fi;
  WEB2=DOWN
  if curl -s -m 5 http://192.168.193.30 >/dev/null; then
    WEB2=UP
  fi
  echo -e "Web1: $WEB1 \t Web2: $WEB2"
done

